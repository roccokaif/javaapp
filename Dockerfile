FROM maven:3.5-jdk-8-alpine
WORKDIR /app
RUN apk add git
RUN git clone https://gitlab.com/ot-interview/javaapp.git /app
RUN mvn package
FROM tomcat:8.0
WORKDIR /app
COPY --from=0 /app/target/Spring3HibernateApp.war /usr/local/tomcat/webapps
EXPOSE 9090
